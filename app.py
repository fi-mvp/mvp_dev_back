from services.generatetoken import TokenGenerator
from services.personalization import PersonalizationManager
from services.getslicers import SlicerManager
from config.pbiconfigcheck import CheckPBIConfig
from flask import Flask, render_template, send_from_directory
from flask import request
import os
from flask_cors import CORS


# Initialize the Flask app
app = Flask(__name__)
CORS(app)

# Branch2 fr126 vm ++ AS
# Load configuration
app.config.from_object('config.pbiconfig.BaseConfig')
app.config['JSON_SORT_KEYS'] = False


@app.route('/getembedinfoall', methods=['POST'])
def get_embed_info_all():
    """Returns report embed configuration"""

    config_result = CheckPBIConfig.check_config(app)

    if config_result is not None:
        return {'errorMsg': config_result}, 500

    embed_info = TokenGenerator().token_generator(app.config['REPORT_MASTER'],
                                                  app.config['WORKSPACE_ID'],
                                                  request.headers.get("userid"),
                                                  request.headers.get("reportid"),
                                                  request.headers.get("core"))

    return embed_info


@app.route('/getpersonalizationdetails', methods=['GET'])
def get_personalization_details():

    theme_layout_info = PersonalizationManager(request.headers.get("userid")).get_details(
        request.headers.get("reportid")
    )

    return theme_layout_info


@app.route('/savepersonalizationdetails', methods=['POST'])
def save_personalization_details():

    theme_layout_info = PersonalizationManager(request.headers.get("userid")).save_details(
        request.headers.get("reportid"),
        request.headers.get("layout"),
        request.headers.get("theme"),
        request.headers.get("last_report"),
        request.headers.get("last_slicer")
    )

    return theme_layout_info


@app.route('/getslicerdetails', methods=['GET'])
def get_slicer_details():

    theme_layout_info = SlicerManager(request.headers.get("userid")).get_details(
        request.headers.get("reportid"),
        request.headers.get("slicer"),
    )

    return theme_layout_info


@app.route('/saveslicerdetails', methods=['POST'])
def save_slicer_details():

    theme_layout_info = SlicerManager(request.headers.get("userid")).save_details(
        request.headers.get("reportid"),
        request.headers.get("slicer"),
        request.get_json(force=True)
    )

    return theme_layout_info


@app.route('/')
def index():
    """Returns a static HTML page"""

    return render_template('index.html')


@app.route('/favicon.ico', methods=['GET'])
def getfavicon():
    """Returns path of the favicon to be rendered"""

    return send_from_directory(os.path.join(app.root_path, 'static'), 'img/favicon.ico',
                               mimetype='image/vnd.microsoft.icon')


if __name__ == '__main__':
    # app.run()
    app.run(host="localhost", port=8000, debug=True)
