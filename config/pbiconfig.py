# Copyright (c) Microsoft Corporation.
# Licensed under the MIT license.

class BaseConfig(object):

    # Can be set to 'MasterUser' or 'ServicePrincipal'
    AUTHENTICATION_MODE = 'MasterUser'

    # Workspace Id in which the report is present
    # WORKSPACE_ID = '941f499f-e458-4cf6-bd4a-aea6e978ebf3'
    WORKSPACE_ID = "6d200fa8-adc8-476e-853b-16b422625f9a"

    # Report Id for which Embed token needs to be generated
    # REPORT_ID = "0880b83b-2fbc-4e39-84d8-1e144fcf3d6f"
    REPORT_ID = "7d183cc3-33b1-4979-9abb-3f1270f729a5"

    # Reports Master
    # keep the report name in lower case
    REPORT_MASTER = {
        "user_sales_db": {
            "has_security": True,
            "rptid": "cc2550f3-158d-412c-9e47-97e1652d2d37",
            "security_group": "UserSecurites"
        },
        "public_report": {
            "has_security": False,
            "rptid": "0880b83b-2fbc-4e39-84d8-1e144fcf3d6f"
        },
        "public_sort": {
            "has_security": False,
            "rptid": "7d183cc3-33b1-4979-9abb-3f1270f729a5"
        }
    }

    # Id of the Azure tenant in which AAD app and Power BI report is hosted.
    # Required only for ServicePrincipal authentication mode.
    TENANT_ID = ''
    
    # Client Id (Application Id) of the AAD app
    # CLIENT_ID = '72407889-5eb7-4c87-9743-a345da2b63d2'
    CLIENT_ID = "eaadfbd2-3aec-4026-907f-85f93beda616"

    # Client Secret (App Secret) of the AAD app. Required only for ServicePrincipal authentication mode.
    CLIENT_SECRET = ''
    
    # Scope of AAD app.
    # Use the below configuration to use all the permissions provided in the AAD app through Azure portal.
    SCOPE = ['https://analysis.windows.net/powerbi/api/.default']
    
    # URL used for initiating authorization request
    AUTHORITY = 'https://login.microsoftonline.com/organizations'

    # Master user email address. Required only for MasterUser authentication mode.
    # POWER_BI_USER = 'Demo_PowerBI@Axtria.Onmicrosoft.com'
    POWER_BI_USER = "FIDev_PowerBI@axtria.onmicrosoft.com"

    # Master user email password. Required only for MasterUser authentication mode.
    POWER_BI_PASS = 'FBnov@2021'
