from services.pbi.pbiembedservice import PbiEmbedService


class TokenGenerator:

    @staticmethod
    def token_generator(report_master, workspace_id, user_id, report_id=None, core="false"):

        try:

            user_id = str(user_id).strip().lower()
            report_id = str(report_id).strip().lower()

            if not(user_id == "none"):

                embed_info_all = {}
                if (report_id is None) or (report_id == "none"):
                    all_reports = list(dict.fromkeys(list(report_master.keys())))
                else:
                    all_reports = [report_id]

                for report_name in all_reports:

                    if report_master[report_name]["has_security"]:
                        security_group = report_master[report_name]["security_group"]
                    else:
                        security_group = None

                    embed_info = PbiEmbedService().get_embed_params_all(workspace_id,
                                                                        report_master[report_name]["rptid"],
                                                                        user_id,
                                                                        security_group)

                    embed_info_all.update({report_name: embed_info})

                if not((report_id is None) or (report_id == "none")):
                    if core == "true":
                        embed_info_all = embed_info_all[report_id]

                return embed_info_all

            else:
                return {'errorMsg': str("User name not found")}, 500

        except Exception as ex:
            return {'errorMsg': str(ex)}, 500
