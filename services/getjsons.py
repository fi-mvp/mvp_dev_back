import pathlib
import json
import os


class GetJsons:

    def __init__(self, user_id):

        users_path = pathlib.Path(__file__).parent.parent.resolve()
        data = json.load(open(os.path.join(users_path, "db", "users.json")))
        if user_id in list(data.keys()):
            self.uid = str(data[str(user_id).lower().strip()]).lower().strip()
            self.uid_valid = True
        else:
            self.uid = ""
            self.uid_valid = False

        self.path_slicers = os.path.join(users_path, "db", "slicers", self.uid + ".json")
        self.path_details = os.path.join(users_path, "db", "details", self.uid + ".json")

    def data_details(self):

        f, err, data = self.__data_fetch(self.path_details)
        return f, err, data

    def data_slicers(self):

        f, err, data = self.__data_fetch(self.path_slicers)
        return f, err, data

    @staticmethod
    def __data_fetch(path):

        data = {}
        f = False
        err = ""
        try:
            if os.path.exists(path) and os.path.isfile(path):
                data = json.load(open(path))
                f = True
            else:
                err = "File not found"
        except Exception as e:
            err = str(e)

        return f, err, data

    @staticmethod
    def check_exists(path):

        f = False
        err = ""
        try:
            if os.path.exists(path) and os.path.isfile(path):
                f = True
            else:
                with open(path, 'w') as fp:
                    json.dump({}, fp, indent=2)
                    f = True
        except Exception as e:
            err = str(e)

        return f, err

    def dump_json(self, path, data):
        f, err = self.check_exists(path)
        try:
            with open(path, 'w') as fp:
                json.dump(data, fp, indent=2)
                f = True
        except Exception as e:
            f = False
            err = err + " | " + str(e)

        return f, err
