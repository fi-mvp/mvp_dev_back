from services.getjsons import GetJsons


class SlicerManager:

    def __init__(self, user_id):

        self.user_id = str(user_id).strip().lower()
        self.valid_uid = False
        if not(self.user_id == "none"):
            self.obj_user = GetJsons(self.user_id)
            if self.obj_user.uid_valid:
                self.valid_uid = True
        else:
            self.obj_user = None

    def get_details(self, report_id, slicer_name):

        try:

            if self.valid_uid:

                f, err, data = self.obj_user.data_slicers()
                if f:
                    keys = list(data.keys())
                    report_id = str(report_id).strip().lower()

                    if not(report_id == "none"):
                        if report_id in keys:
                            ret = {report_id: data[report_id]}
                            if not (str(slicer_name).strip().lower() == "none"):
                                if slicer_name in list(ret[report_id].keys()):
                                    ret = {report_id: {slicer_name: data[report_id][slicer_name]}}
                                    error = ""
                                    flag = True
                                else:
                                    ret = {report_id: {slicer_name: {}}}
                                    error = "Slicer not found for the report"
                                    flag = False
                            else:
                                error = ""
                                flag = True
                        else:
                            ret = {report_id: {}}
                            error = "Report not found"
                            flag = False
                    else:
                        ret = data
                        error = ""
                        flag = True

                    ret.update({"success": flag, "error": error})

                else:
                    ret = {"success": False, "error": err}
            else:
                ret = {"success": False, "error": "Given user is invalid"}

        except Exception as e:
            ret = {"success": False, "error": str(e)}

        return ret

    def save_details(self, report_id, slicer_name, slicer_details):

        try:

            if self.valid_uid:

                f, err, data = self.obj_user.data_slicers()
                if f:
                    keys = list(data.keys())
                    report_id = str(report_id).strip().lower()

                    if not ((report_id == "none") or (str(slicer_name).strip().lower() == "none") or (
                            str(slicer_details).strip().lower() == "none")):
                        if report_id in keys:
                            c = data[report_id]
                            c.update({slicer_name: slicer_details})
                            data.update({report_id: c})
                            error = ""
                            flag = True
                        else:
                            data.update({report_id: {slicer_name: slicer_details}})
                            error = ""
                            flag = True
                    else:
                        error = "Input details missing"
                        flag = False

                    if flag:
                        flag, error = self.obj_user.dump_json(self.obj_user.path_slicers, data)

                    ret = {"success": flag, "error": error}

                else:
                    ret = {"success": False, "error": err}
            else:
                ret = {"success": False, "error": "Given user is invalid"}

        except Exception as e:
            ret = {"success": False, "error": str(e)}

        return ret
