# Copyright (c) Microsoft Corporation.
# Licensed under the MIT license.

from services.pbi.aadservice import AadService
from models.reportconfig import ReportConfig
from models.embedtoken import EmbedToken
from models.embedconfig import EmbedConfig
from models.embedtokenrequestbody import EmbedTokenRequestBody
from flask import abort
import requests
import json


class PbiEmbedService:

    def get_embed_params_all(self, workspace_id, report_id, user_id, security_group=None):
        """Get embed params for a report and a workspace

        Args:
            workspace_id (str): Workspace Id
            report_id (str): Report Id
            user_id: id of user to give access
            security_group (str, optional): security group for RLS token

        Returns:
            EmbedConfig: Embed token and Embed URL
        """

        report_url = f'https://api.powerbi.com/v1.0/myorg/groups/{workspace_id}/reports/{report_id}'
        api_response = requests.get(report_url, headers=self.get_request_header())

        if api_response.status_code != 200:
            e = f'\n{api_response.reason}:\t{api_response.text}\nRequestId:\t{api_response.headers.get("RequestId")}'
            e = f'Error while retrieving Embed URL' + e
            abort(api_response.status_code, description=e)

        api_response = json.loads(api_response.text)
        report = ReportConfig(api_response['id'], api_response['name'], api_response['embedUrl'])
        dataset_ids = [api_response['datasetId']]

        embed_token = self.get_embed_token(report_id, dataset_ids, user_id, security_group, workspace_id)
        embed_config = EmbedConfig(embed_token.tokenId, embed_token.token, embed_token.tokenExpiry, [report.__dict__])
        return embed_config.__dict__

    def get_embed_token(self, report_id, dataset_ids, user_id, security_group, target_workspace_id=None):
        """Get Embed token for single report, multiple datasets, and an optional target workspace

        Args:
            report_id (str): Report Id
            dataset_ids (list): Dataset Ids
            user_id: id of the user to give access
            security_group: name of the security group for RLS
            target_workspace_id (str, optional): Workspace Id. Defaults to None.

        Returns:
            EmbedToken: Embed token
        """

        request_body = EmbedTokenRequestBody()

        for dataset_id in dataset_ids:
            request_body.datasets.append({'id': dataset_id})

        request_body.reports.append({'id': report_id})

        if target_workspace_id is not None:
            request_body.targetWorkspaces.append({'id': target_workspace_id})

        # Generate Embed token for multiple workspaces, datasets, and reports.
        # Refer https://aka.ms/MultiResourceEmbedToken
        embed_token_api = 'https://api.powerbi.com/v1.0/myorg/GenerateToken'

        api_headers = self.get_request_header()

        api_body_rls = request_body.__dict__
        if security_group is not None:
            api_body_rls.update(
                {
                    "accessLevel": "View",
                    "identities": [
                        {
                            "username": str(user_id),
                            "roles": [security_group],
                            "datasets": [api_body_rls["datasets"][0]["id"]]
                        }
                    ]
                }
            )
        api_body_rls = json.dumps(api_body_rls)

        # api_response = requests.post(embed_token_api, data=api_body, headers=api_headers)
        api_response = requests.post(embed_token_api, data=api_body_rls, headers=api_headers)

        if api_response.status_code != 200:
            e = f'\n{api_response.reason}:\t{api_response.text}\nRequestId:\t{api_response.headers.get("RequestId")}'
            e = f'Error while retrieving Embed token' + e
            abort(api_response.status_code, description=e)

        api_response = json.loads(api_response.text)
        embed_token = EmbedToken(api_response['tokenId'], api_response['token'], api_response['expiration'])
        return embed_token

    @staticmethod
    def get_request_header():
        """Get Power BI API request header

        Returns:
            Dict: Request header
        """

        return {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + AadService.get_access_token()}
