from services.getjsons import GetJsons


class PersonalizationManager:

    def __init__(self, user_id):

        self.user_id = str(user_id).strip().lower()
        self.valid_uid = False
        if not(self.user_id == "none"):
            self.obj_user = GetJsons(self.user_id)
            if self.obj_user.uid_valid:
                self.valid_uid = True
        else:
            self.obj_user = None

    def get_details(self, report_id):

        try:

            if self.valid_uid:

                f, err, data = self.obj_user.data_details()
                if f:
                    ret = {}
                    keys = list(data.keys())
                    if "current_theme" in keys:
                        ret.update({"current_theme": data["current_theme"]})
                    if "layouts" in keys:
                        report_id = str(report_id).strip().lower()
                        if (str(report_id) in list(data["layouts"].keys())) and not(report_id == "none"):
                            ret.update({str(report_id): data["layouts"][str(report_id)]})
                        else:
                            ret.update(data["layouts"])
                    if "last" in keys:
                        if "report" in list(data["last"].keys()):
                            ret.update({"last_dashboard": data["last"]["report"]})
                            if "slicer" in list(data["last"].keys()):
                                ret.update({"last_dashboard_slicer": data["last"]["slicer"]})

                    ret.update({"success": True, "error": err})

                else:
                    ret = {"success": False, "error": err}
            else:
                ret = {"success": False, "error": "Given user is invalid"}

        except Exception as e:
            ret = {"success": False, "error": str(e)}

        return ret

    def save_details(self, report_id, layout, theme, last_report, last_slicer):

        try:

            if self.valid_uid:

                f, err, data = self.obj_user.data_details()
                if f:

                    keys = list(data.keys())
                    themes_info = False
                    layout_info = False
                    last_info = False

                    if not (str(theme).strip().lower() == "none"):
                        data.update({"current_theme": theme})
                        themes_info = True

                    if not (str(report_id).strip().lower() == "none") and \
                            not (str(layout).strip().lower() == "none"):
                        if "layouts" in keys:
                            new_layouts = data["layouts"]
                        else:
                            new_layouts = {}
                        new_layouts.update({str(report_id).strip().lower(): layout})
                        data.update({"layouts": new_layouts})
                        layout_info = True

                    if not (str(last_report).strip().lower() == "none"):
                        if "last" in keys:
                            new_last = data["last"]
                        else:
                            new_last = {}
                        new_last.update({"report": str(last_report).strip().lower()})
                        if not (str(last_slicer).strip().lower() == "none"):
                            new_last.update({"slicer": last_slicer})
                        data.update({"last": new_last})
                        last_info = True

                    f, err = self.obj_user.dump_json(self.obj_user.path_details, data)

                    if f:
                        ret = {"themes": themes_info, "layout": layout_info, "last": last_info}
                    else:
                        ret = {"success": False, "error": err}

                else:
                    ret = {"success": False, "error": err}
            else:
                ret = {"success": False, "error": "Given user is invalid"}

        except Exception as e:
            ret = {"success": False, "error": str(e)}

        return ret
