function loadReportCaller(typ, boot=false) {

    if (typ == 1) {
        user_name = "A0511@AXTRIA.COM";
        report_name = "public_report";
    } else if (typ == 2) {
        user_name = "A0511@AXTRIA.COM";
        report_name = "user_sales_db";
    } else if (typ == 3) {
        user_name = "A0511@AXTRIA.COM";
        report_name = "public_sort";
    }

    loadReport(report_name, user_name, boot).catch(e => {
      console.log('There has been a problem with the operation: ' + e.message);
    });

}

async function loadReport(report_name, user_name, boot) {

    var reportContainer = reportContainerObject.get(0);

    if (boot == true) {
        console.log("Initialize iframe for embedding report");
        powerbi.bootstrap(reportContainer, { type: "report" });
    }

    var models = window["powerbi-client"].models;

    var permissions = models.Permissions.View;
    //var permissions = models.Permissions.All;

    var settings = {
        // background: models.BackgroundType.Transparent,
        layoutType: models.LayoutType.Custom,
        panes: {
            filters: {
                expanded: false,
                visible: false
            },
            pageNavigation: {
                visible: false
            }
        },
        customLayout: {
             pageSize: models.PageSizeType.Widescreen
        }
    };

    var reportLoadConfig = {
            type: "report",
            tokenType: models.TokenType.Embed,
            settings: settings,
            permissions: permissions
    };

    $.ajax({
        type: "POST",
        url: "/getembedinfoall",
        dataType: "json",
        headers: { 'Content-Type': 'application/json', 'userid': String(user_name), "reportid": String(report_name), "core": "true" },
        success: function (data) {
            embedData = $.parseJSON(JSON.stringify(data));

            reportLoadConfig.accessToken = embedData.accessToken;
            reportLoadConfig.embedUrl = embedData.reportConfig[0].embedUrl;

            // Use the token expiry to regenerate Embed token for seamless end user experience
            // Refer https://aka.ms/RefreshEmbedToken
            tokenExpiry = embedData.tokenExpiry;

            // Embed Power BI report when Access token and Embed URL are available
            //var report = powerbi.embed(reportContainer, reportLoadConfig);
            reportObject.report = powerbi.embed(reportContainer, reportLoadConfig);
            setReportAccessibilityProps(reportObject.report, "Field Intelligence Demo");

            // Triggers when a report schema is successfully loaded
            reportObject.report.on("loaded", function () {
                console.log("Report load successful")
            });

            // Triggers when a report is successfully embedded in UI
            reportObject.report.on("rendered", function () {
                console.log("Report render successful")
            });

            // Clear any other error handler event
            reportObject.report.off("error");

            // Below patch of code is for handling errors that occur during embedding
            reportObject.report.on("error", function (event) {
                var errorMsg = event.detail;

                // Use errorMsg variable to log error in any destination of choice
                console.error(errorMsg);
                return;
            });
        },
        error: function (err) {

            // Show error container
            var errorContainer = $(".error-container");
            $(".embed-container").hide();
            errorContainer.show();

            // Format error message
            var errMessageHtml = "<strong> Error Details: </strong> <br/>" + $.parseJSON(err.responseText)["errorMsg"];
            errMessageHtml = errMessageHtml.split("\n").join("<br/>")

            // Show error message on UI
            errorContainer.html(errMessageHtml);
        }
    });

}

// Set props for accessibility insights
function setReportAccessibilityProps(report, msg) {
    report.setComponentTitle(msg);
    report.setComponentTabIndex(0);
}
