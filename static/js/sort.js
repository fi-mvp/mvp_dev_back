function setAyncCallListElements() {
    listElements().catch(e => {
      console.log('There has been a problem with the operation: ' + e.message);
    });
}

async function listElements() {

    var errMsg = "";
    try {
        const pages = await reportObject.report.getPages();

        if (pages.length === 0) {
            errMsg = 'No pages found.';
            console.log(errMsg);
        } else {

            // List all pages
            var pageList = [];
            var pageListO = [];
            var active_page;
            var active_page_found = false;
            for (var p=0; p<pages.length; p++) {
                pageList.push({name: pages[p].name, displayName: pages[p].displayName, active: pages[p].isActive});
                pageListO.push({name: pages[p]});
                if (pages[p].isActive == true) {
                   active_page = pages[p];
                   active_page_found = true;
                }
            }
            // console.log(pageList);

            if (active_page_found == true) {

                var visualsList = [];
                var visualsListO = [];
                const visuals = await active_page.getVisuals();
                var models = window["powerbi-client"].models;
                for (var p=0; p<visuals.length; p++) {
                    visualsList.push({name: visuals[p].name, title: visuals[p].title, type: visuals[p].type, pageName: visuals[p].page.name, pageDisplayName: visuals[p].page.displayName, pageActive: visuals[p].page.isActive});
                    visualsListO.push({name: visuals[p]});
                    if (String(visuals[p].title).startsWith("Stacked Col - ")) {
                        targetVisual = visuals[p];
                        console.log("'" + visuals[p].title + "' assigned as targetVisual");
                    }
                }
                console.log(visualsListO);

            }

        }
    }
    catch (errors) {
        console.log(errors);
    }
}

function sortGraph(tbl, cm, col, odr) {
    sortFunctionCaller(tbl, cm, col, odr).catch(e => {
      console.log('There has been a problem with the operation: ' + e.message);
    });
}

async function sortFunctionCaller(tbl, cm, col, odr) {

    var models = window["powerbi-client"].models;

    var sortRequest = {};
    var sortDirection;

    if (String(odr).trim().toLowerCase() == "d") {
        sortDirection = models.SortDirection.Descending
    } else if (String(odr).trim().toLowerCase() == "a") {
        sortDirection = models.SortDirection.Ascending
    }

    if (String(cm).trim().toLowerCase() == "c") {
        sortRequest = {
            orderBy: {
                // $schema: "http://powerbi.com/product/schema#columnAggr",
                table: tbl,
                column: col
            },
            direction: sortDirection
        }
    } else if (String(cm).trim().toLowerCase() == "m") {
        sortRequest = {
            orderBy: {
                table: tbl,
                measure: col
            },
            direction: sortDirection
        }
    }

    console.log(sortRequest);
    console.log(targetVisual.title);
    try {
        await targetVisual.sortBy(sortRequest);
    } catch (errors) {
        console.log(errors);
    }
}
